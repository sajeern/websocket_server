// http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
"use strict";

// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'node-chat';

var config = require('./config');

// Port where we'll run the websocket server
var webSocketsServerPort = config.ServerPort;

// Wait for seconds till server finishes job
var waitTime = config.WaitTime * 1000;

// websocket and http servers
var webSocketServer = require('websocket').server;
var http = require('http');
var https = require('https');

// To write console.log to file
var fs = require('fs');
var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) {
  log_file.write(util.format(d) + '\n');
  log_stdout.write(util.format(d) + '\n');
};

/**
 * Global variables
 */
// Logged in users user ids in clients
var userIds;

// list of currently connected clients (users)
var clients = [ ];

/**
 * Helper function for escaping input strings
 */
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                      .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

/**
 * HTTP server
 */
var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});
server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
});

var actionMethods = {};

/**
 * Method to initialize client and register client
 */
actionMethods.clientInitialize = function(parsedData, connection) {
    if (parsedData.userId != 1) { // To avoid checking admin users
        connection.userObj = {
            'time': (new Date()).getTime(),
            'clientSSID': parsedData.clientSSID,
            'userId': parsedData.userId,
        };
    }
}

/**
 * Notify all clients with the message. Delay the notification based on the parameter
 */
actionMethods.notifyClients = function(parsedData, connection) {
    connection.sendUTF('data received');
    console.log('Received new message to broadcast');

    // Delay for 5 seconds so that server executes everything
    if (parsedData.require_delay == true) {
        console.log('---------------------Delaying for ' + waitTime + ' microseconds---------------------------');
        setTimeout(function() {
            sendNotificationToClients(parsedData);
        }, waitTime);
    }
    else {
        console.log('---- NO DELAY IN EXECUTION---------------');
        sendNotificationToClients(parsedData);
    }
}

/**
 * Method to call notification API in drupal when a comment is added/edited
 */
actionMethods.sendCommentNotification = function(parsedData, connection) {
    connection.sendUTF('data received');
    console.log('-------------------------------------------------------------');
    console.log('Comment notification received...............................');
    var postData = JSON.stringify({
        tp_id: parsedData.tp_id,
        uid: parsedData.uid,
        action: parsedData.action
    });

    var request = require('request');
    request({
        url: config.CommnetAPIUrl,
        method: 'POST',
        headers: {
            "id-token": config.ApiIDToken,
            "Content-Type": "application/json"
        },
        body: postData,
        }, function (error, response, body){
            console.log(postData);
            console.log(response.statusCode);
            if(body == undefined || body == null) {
                logger.log('error', 'Notification failed', { "request": metaData, "response": response });
            }
    });
    request = null;
    postData = null;
}

function sendNotificationToClients(parsedData) {
    var userId;
    userIds = [];

    console.log('----------------------------------------------------------');
    console.log('----------------------------------------------------------');
    console.log(parsedData);

    console.log(' Number of clients: ' + (clients.length - 1));
    for (var i = 0; i < clients.length; i++) {
        if (clients[i].userObj) {
            userIds.push(clients[i].userObj.userId);
            try {
                userId = clients[i].userObj.userId;
                if (typeof(parsedData.user_ids[0]) == 'string')
                    userId = userId + '';
                else
                    userId = userId * 1;

                if (parsedData.user_ids.indexOf(userId) >= 0) {
                    var notifyMsgArray = {
                        'notifyMsg': parsedData.message,
                        'userIds': parsedData.user_ids,
                        'link': parsedData.target_link,
                        'title': parsedData.title,
                        'clientSSID': clients[i].userObj.clientSSID
                    };
                    clients[i].sendUTF(JSON.stringify(notifyMsgArray));
                    console.log(" --Done for: " + userId);
                }
            }
            catch(err) {
                console.log('could not send' + err);
            }
        }
    }

    console.log('existing users ids---------');
    console.log(userIds);
    console.log('----------------------------------------------------------');
    console.log('----------------------------------------------------------');
}

// This callback function is called every time someone
// tries to connect to the WebSocket server
wsServer.on('request', function(request) {
    console.log((new Date()) + ' Connection from origin ' + request.origin + '.');

    // TODO: check the origin as thread.qburst.com OR thread.org
    var connection = request.accept(null, request.origin);
    // we need to know client index to remove them on 'close' event
    var index = clients.push(connection) - 1;

    console.log((new Date()) + ' Connection accepted.');

    // user sent some message
    connection.on('message', function(message) {
        try {
            if (message.type === 'utf8') { // accept only text
                var parsedData = JSON.parse(message.utf8Data);

                actionMethods[parsedData.method](parsedData, connection);
            }
        }
        catch(err) {
            connection.sendUTF(err);
            console.log('wrong message data obtained' + err);
        }
    });

    // user disconnected
    connection.on('close', function(connection) {
        console.log((new Date()) + " Peer " + connection.remoteAddress + " disconnected.");
        // remove user from the list of connected clients
        clients.splice(index, 1);
    });

});
